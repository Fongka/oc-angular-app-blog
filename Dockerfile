# Stage 0, based on Node.js, to build and compile Angular
FROM node:latest as node
WORKDIR /app
COPY package.json /app/
RUN npm install
COPY ./ /app/
RUN ./node_modules/.bin/ng build
# ARG env=prod
# RUN ./node_modules/.bin/ng build $env

# Stage 1, based on Nginx,
FROM nginx:1.13
COPY --from=node /app/dist/ /usr/share/nginx/html
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf
